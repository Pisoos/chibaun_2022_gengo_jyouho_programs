#!/usr/bin/perl

package logger;

use Exporter;

our @ISA = qw(Exporter);
our @EXPORT = qw(log_system log_system_m log_system_err log_hanbaiki);

sub log_system
{
	my $msg = shift;
	print "\033[0;37m[SYSTEM] $msg\033[0;0m\n"
}
sub log_system_err
{
	my $msg = shift;
	print "\033[0;31m[SYSTEM] $msg\033[0;0m\n";
}
sub log_system_m
{
	my $msg = shift;
	$msg =~ s/\n/\n[SYSTEM] /ig;
	log_system($msg);
}
sub log_hanbaiki
{
	my $msg = shift;
	print "\033[0;32m[HANBAIKI] $msg\033[0;0m\n"
}

1;

