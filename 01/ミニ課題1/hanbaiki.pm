#!/usr/bin/perl

package hanbaiki_t;

use lib "../";
use logger;

sub new
{
	my $class = shift;
	my $self = {
		_sum => 0,
	};
	
	bless $self, $class;

	log_system_m("これはシステムの出力であり、販売機の出力と異なる。
販売機の出力は緑色の文字で、[HANBAIKI]接頭辞から始まる。");

	return $self;
}


sub proceed_input_and_get_output
{
	my ($self, $input) = @_;

	if ($input == "1")
	{
		log_system("もらった入力：10円玉");
		$self->{_sum} += 10;
	}
	else
	{
		log_system("もらった入力：50円玉");
		$self->{_sum} += 50;
	}

	log_system("現在状態：$self->{_sum}円蓄");

	if ($self->{_sum} >= 60)
	{
		$self->{_sum} = 0;
		log_hanbaiki("珈琲");
		log_system("現在状態：$self->{_sum}円蓄");
	}
}

sub get_input
{
	my ($self) = @_;

	log_system("入力してください（数字）
1. 10円玉
2. 50円玉");
	my $input = "";
	while (true) {
		$input = <STDIN>;
		chomp $input;

		if ($input == "1" || $input == "2")
		{
			last;
		}

		log_system_err("正しい入力をしてください(1,2)");
	}

	$self->proceed_input_and_get_output($input);
}

1;

