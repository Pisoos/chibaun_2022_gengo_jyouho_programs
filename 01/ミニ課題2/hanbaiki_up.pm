#!/usr/bin/perl

package hanbaiki_up_t;

use feature qw(switch);

use lib "../";
use logger;


sub new
{
	my $class = shift;
	my $self = {
		_sum => 0,
	};
	
	bless $self, $class;

	log_system_m("これはシステムの出力であり、販売機の出力と異なる。
販売機の出力は緑色の文字で、[HANBAIKI]接頭辞から始まる。");

	return $self;
}

sub try_to_get_something
{
	my ($self, $input) = @_;

	if ($self->{_sum} < 60)
	{
		return;
	}

	$self->{_sum} = 0;

	if ($input == "3")
	{
		log_hanbaiki("珈琲");
		return;
	}
	log_hanbaiki("紅茶");
}

sub proceed_input_and_get_output
{
	my ($self, $input, $input_str) = @_;

	log_system("もらった入力：$input_str");
	given($input)
	{
		when("1")	{$self->{_sum} += 10;}
		when("2")	{$self->{_sum} += 50;}
		default		{$self->try_to_get_something($input);}	
	}

	log_system("現在状態：$self->{_sum}円蓄");
}

sub get_input
{
	my ($self) = @_;

	log_system("入力してください（数字）
1. 10円玉
2. 50円玉
3. 珈琲ボ
4. 紅茶ボ");
	@input_str = ("10円玉", "50円玉", "珈琲ボ", "紅茶ボ");
	my $input = "";
	while (true) {
		$input = <STDIN>;
		chomp $input;

		if (index("1234", $input) != -1 && length($input) == 1)
		{
			last;
		}

		log_system_err("正しい入力をしてください(1,2)");
	}

	$self->proceed_input_and_get_output($input, @input_str[$input - 1]);
}

1;

